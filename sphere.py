#!/usr/bin/python

def print_volume(r):
    """
        print_volume

        print volume of a sphere (4/3)*pi*(r^3)

        parameters

        r(float): radius of a sphere

        file sphere.py
    """
    cube = r*r*r
    print float(1.33*3.14*cube)
