#!/usr/bin/python

from sphere import print_volume

if __name__ == "__main__":
    ra,rb,rc = 3,4,5
    print_volume(ra)
    print_volume(rb)
    print_volume(rc)

    """
    input

    first call 3
    second call 4
    third call 5

    output

    first call 112.7574
    second call 267.2768
    third call 522.025

    file qa.py
    """

