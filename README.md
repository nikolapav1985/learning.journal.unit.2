Learning journal unit 2
-----------------------

Examples regarding learning journal unit 2.

Included files
--------------

sphere.py (a function to print sphere volume), gcd.py (a function to find greatest common divisor of couple of numbers), qa.py (test print_volume function), qb.py (test GCD function).

Test environment
----------------

OS lubuntu 16.04 lts kernel version 4.13.0. Python version 2.7.12.

Example test
------------

Type ./qa.py (or any other file).
