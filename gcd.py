#!/usr/bin/python

def gcd(a,b):
    """
        gcd

        find greatest common divisor GCD of couple of numbers a,b using Euclid algorithm
        that is, name and later run steps to find GCD of couple of numbers
        basic step a=(quot)b+rem quot quotient rem remainder
        repeat basic step until remainder zero
        return b as GCD

        parameters

        a(integer): first number
        b(integer): second number

        a>=b
        a>0
        b>0

        file gcd.py

        return

        gcd(integer): greatest common divisor of couple of numbers
    """
    rem = a%b
    quot = a/b
    while True:
        if rem == 0:
            break
        a=b
        b=rem
        rem = a%b
        quot = a/b
    return b
