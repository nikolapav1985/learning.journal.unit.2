#!/usr/bin/python

from gcd import gcd

if __name__ == "__main__":
    a1,b1 = 51,15
    a2,b2 = 15,3
    a3,b3 = 55,35
    print gcd(a1,b1)
    print gcd(a2,b2)
    print gcd(a3,b3)
    """
    input

    first call arguments 51,15
    second call arguments 15,3
    third call arguments 55,35

    output

    first call 3
    second call 3
    third call 5

    file qb.py
    """
